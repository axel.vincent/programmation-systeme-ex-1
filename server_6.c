#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>

bool running = true;

stop_handler(int sig)
{
    printf("%d\n", sig);
    printf("le message de stop handler");
    running = false;
}

exit_message()
{
    printf("Message d'exit");
}

int main(void)
{
    srand(time(NULL));

    int exitfun = atexit(exit_message);

    /* Initialize the sigaction structure */
    struct sigaction s;
    s.sa_handler = stop_handler;

    int retval = sigaction(SIGINT, &s, NULL);
    int retkill = sigaction(SIGTERM, &s, NULL);
    int retPipe = sigaction(SIGPIPE, &s, NULL);

    printf("le message de départ");

    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);

    server_fd = socket(AF_INET, SOCK_STREAM, 0);
    setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT,
               &opt, sizeof(opt));

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_port = htons(8080);
    // Forcefully attaching socket to the port 8080
    bind(server_fd, (struct sockaddr *)&address,
         sizeof(address));

    listen(server_fd, 3);

    (new_socket = accept(server_fd, (struct sockaddr *)&address,
                         (socklen_t *)&addrlen));
    while (running)
    {
        // The server compute the data and send them
        int id = getpid();
        int parId = getppid();
        int randomValue = rand() % 101;
        sleep(1);

        char buffer1[BUFSIZ];
        char buffer2[BUFSIZ];
        char buffer3[BUFSIZ];

        /* compute the data */
        sprintf(buffer1, "%d", id);

        sprintf(buffer2, "%d", parId);

        sprintf(buffer3, "%d", randomValue);

        char transition[10] = ", and: ";

        strcat(buffer1, transition);
        strcat(buffer1, buffer2);
        strcat(buffer1, transition);
        strcat(buffer1, buffer3);

        send(new_socket, buffer1, strlen(buffer1), 0);
    }
    printf("le message de fin");

    return EXIT_SUCCESS;
}