#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>

bool running = true;

stop_handler(int sig)
{
    printf("%d\n", sig);
    printf("le message de stop handler");
    running = false;
}

exit_message()
{
    printf("Message d'exit");
}

int main(void)
{
    srand(time(NULL));

    int exitfun = atexit(exit_message);

    /* Initialize the sigaction structure */
    struct sigaction s;
    s.sa_handler = stop_handler;

    int retval = sigaction(SIGINT, &s, NULL);
    int retkill = sigaction(SIGTERM, &s, NULL);
    int retPipe = sigaction(SIGPIPE, &s, NULL);

    // Create pipe
    int pfd[2];
    if (pipe(pfd) == -1)
    {
        printf("pipe failed\n");
        return 1;
    }

    pid_t forkValue = fork();
    if (forkValue == -1)
    {
        exit(EXIT_FAILURE);
    }

    printf("le message de départ");
    while (running)
    {

        if (forkValue == 0)
        {
            // First the son display its own message :
            int id = getpid();
            int parId = getppid();
            printf("%d\n", id);
            printf("%d\n", parId);
            int randomValue = rand() % 101;
            printf("%d\n", randomValue);
            sleep(1);

            char buffer[BUFSIZ];

            close(pfd[1]);

            read(pfd[0], buffer, BUFSIZ);
            printf("child reads %s", buffer);
        }
        else
        {
            // The parent compute the data but don't print them
            int id = getpid();
            int parId = getppid();
            int randomValue = rand() % 101;
            sleep(1);

            char buffer1[BUFSIZ];
            char buffer2[BUFSIZ];
            char buffer3[BUFSIZ];

            /* send some data into the pipe */
            sprintf(buffer1, "%d", id);

            sprintf(buffer2, "%d", parId);

            sprintf(buffer3, "%d", randomValue);

            char transition[10] = ", and: ";

            strcat(buffer1, transition);
            strcat(buffer1, buffer2);
            strcat(buffer1, transition);
            strcat(buffer1, buffer3);

            close(pfd[0]);
            write(pfd[1], buffer1, strlen(buffer1) + 1);
        }
    }
    if (forkValue == 0)
    {
        close(pfd[0]);
    }
    else
    {
        close(pfd[1]);
    };
    printf("le message de fin");
    return EXIT_SUCCESS;
}