#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>

bool running = true;

stop_handler(int sig)
{
    printf("%d\n", sig);
    printf("le message de stop handler");
    running = false;
}

exit_message()
{
    printf("Message d'exit");
}

int main(void)
{
    srand(time(NULL));

    int exitfun = atexit(exit_message);

    /* Initialize the sigaction structure */
    struct sigaction s;
    s.sa_handler = stop_handler;

    int retval = sigaction(SIGINT, &s, NULL);
    int retkill = sigaction(SIGTERM, &s, NULL);
    int retPipe = sigaction(SIGPIPE, &s, NULL);

    printf("le message de départ");

    int fd;
    char *myfifo = "/tmp/myfifo";
    // open the FIFO
    fd = open(myfifo, O_RDONLY);

    while (running)
    {
        // The client read the data and print them
        int id = getpid();
        int parId = getppid();
        int randomValue = rand() % 101;
        sleep(1);

        char buffer[BUFSIZ];

        // Read info from the fifo
        ssize_t n = read(fd, buffer, BUFSIZ);
        if (n == 0)
            break;
        printf("The clients prints its own info");
        printf("%d\n", id);
        printf("%d\n", parId);
        printf("%d\n", randomValue);
        printf("And the server has sent the following info: %s", buffer);
    }
    printf("le message de fin");

    // Remove fifo
    close(fd);
    unlink(myfifo);

    return EXIT_SUCCESS;
}