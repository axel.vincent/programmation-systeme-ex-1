#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>

bool running = true;

stop_handler(int sig)
{
    printf("%d\n", sig);
    printf("le message de stop handler");
    running = false;
}

exit_message()
{
    printf("Message d'exit");
}

int main(void)
{
    srand(time(NULL));

    int exitfun = atexit(exit_message);

    /* Initialize the sigaction structure */
    struct sigaction s;
    s.sa_handler = stop_handler;

    int retval = sigaction(SIGINT, &s, NULL);
    int retkill = sigaction(SIGTERM, &s, NULL);
    int retPipe = sigaction(SIGPIPE, &s, NULL);

    printf("le message de départ");

    int fd;
    char *myfifo = "/tmp/myfifo";
    /* create the FIFO */
    mkfifo(myfifo, 0666);
    fd = open(myfifo, O_WRONLY);

    while (running)
    {
        // The server compute the data and send them
        int id = getpid();
        int parId = getppid();
        int randomValue = rand() % 101;
        sleep(1);

        char buffer1[BUFSIZ];
        char buffer2[BUFSIZ];
        char buffer3[BUFSIZ];

        /* send some data into the pipe */
        sprintf(buffer1, "%d", id);

        sprintf(buffer2, "%d", parId);

        sprintf(buffer3, "%d", randomValue);

        char transition[10] = ", and: ";

        strcat(buffer1, transition);
        strcat(buffer1, buffer2);
        strcat(buffer1, transition);
        strcat(buffer1, buffer3);

        /* write the text into the FIFO */
        write(fd, buffer1, sizeof(buffer1));
    }
    printf("le message de fin");

    /* remove the FIFO */
    close(fd);
    unlink(myfifo);
    return EXIT_SUCCESS;
}