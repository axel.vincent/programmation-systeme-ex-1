#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>

bool running = true;

stop_handler(int sig)
{
    printf("%d\n", sig);
    printf("le message de stop handler");
    running = false;
}

exit_message()
{
    printf("Message d'exit");
}

int main(void)
{
    int exitfun = atexit(exit_message);

    srand(time(NULL));
    printf("le message de départ");
    while (running)
    {

        /* Initialize the sigaction structure */
        struct sigaction s;
        s.sa_handler = stop_handler;

        int retval = sigaction(SIGINT, &s, NULL);
        int retkill = sigaction(SIGTERM, &s, NULL);

        int id = getpid();
        int parId = getppid();
        printf("%d\n", id);
        printf("%d\n", parId);
        int randomValue = rand() % 101;
        printf("%d\n", randomValue);
        sleep(1);
    }
    printf("le message de fin");
    return EXIT_SUCCESS;
}

// Lorsque le programme est stoppé avec l'appel de la commande kill sans paramètre
// Le message de stop_handler s'affiche. Si kill est appelé sans paramètre, il
// n'y a pas de message affiché.

// On ne peut pas faire en sorte que le message s'affiche avec le paramètre - s KILL
// car les signaux kill ne sont pas interceptable

// Si on kill le processus père il ne se passe rien. Cepandant lorsque l'on utilise
// la commande kill -s KILL sur le processus père, le programme et le terminal
// s'arrête. Il faut ouvrir un nouveau terminal 1 pour continuer.

// Lorsque la fonction stop_handler ne modifie pas running le programme ne
// s'arrete pas lorsqu'on utilise cmd + C ou kill. Mais il s'arrête avec la
// commande kill -9

// Le message généré par la fonction exit_message() est bien affiché lorsque l'on
// termine le programme avec cmd + C ou kill. Mais pas si on le termine avec
// kill -9.