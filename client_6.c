#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <arpa/inet.h>

bool running = true;

stop_handler(int sig)
{
    printf("%d\n", sig);
    printf("le message de stop handler");
    running = false;
}

exit_message()
{
    printf("Message d'exit");
}

int main(void)
{
    srand(time(NULL));

    int exitfun = atexit(exit_message);

    /* Initialize the sigaction structure */
    struct sigaction s;
    s.sa_handler = stop_handler;

    int retval = sigaction(SIGINT, &s, NULL);
    int retkill = sigaction(SIGTERM, &s, NULL);
    int retPipe = sigaction(SIGPIPE, &s, NULL);

    printf("le message de départ");

    int sock = 0, valread;
    struct sockaddr_in serv_addr;
    char buffer[1024] = {0};
    sock = socket(AF_INET, SOCK_STREAM, 0);

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(8080);
    inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr);
    connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr));

    while (running)
    {
        // The client read the data and print them
        int id = getpid();
        int parId = getppid();
        int randomValue = rand() % 101;
        sleep(1);

        char buffer[BUFSIZ];

        valread = read(sock, buffer, 1024);

        printf("The clients prints its own info");
        printf("%d\n", id);
        printf("%d\n", parId);
        printf("%d\n", randomValue);
        printf("And the server has sent the following info: %s", buffer);
    }
    printf("le message de fin");

    return EXIT_SUCCESS;
}