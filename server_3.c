#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdbool.h>

bool running = true;

stop_handler(int sig)
{
    printf("%d\n", sig);
    printf("le message de stop handler");
    running = false;
}

exit_message()
{
    printf("Message d'exit");
}

int main(void)
{
    int exitfun = atexit(exit_message);

    srand(time(NULL));
    pid_t forkValue = fork();
    if (forkValue == -1)
    {
        exit(EXIT_FAILURE);
    }
    else
    {
        wait(NULL);
    };
    printf("le message de départ");
    while (running)
    {

        /* Initialize the sigaction structure */
        struct sigaction s;
        s.sa_handler = stop_handler;

        int retval = sigaction(SIGINT, &s, NULL);
        int retkill = sigaction(SIGTERM, &s, NULL);

        int id = getpid();
        int parId = getppid();
        printf("%d\n", id);
        printf("%d\n", parId);
        int randomValue = rand() % 101;
        printf("%d\n", randomValue);
        sleep(1);
    }
    printf("le message de fin");
    return EXIT_SUCCESS;
}

// Il est possible de distinguer les messages du père de ceux du fils car les
// messages du fils affichent l'id de leur père avec getppid (Valeur que l'on
// connait car le père affiche son id avec getpid).
// Les deux processus vont s'arrêter avec un appel de ctrl + C

// Si on arrête le processus fils, il est toujours affiché dans la liste suite
// à la commande ps a, mais le processus n'est plus actif comme les logs ne
// s'affiche plus dans le terminal 1 pour le processus fils.
// Si on arrête le père, les logs du fils continuent de se déclencher mais le fils
// est associé à un nouveau processus parent qui a l'id 1
